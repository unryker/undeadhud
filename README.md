### Philosophy
---
- Be somewhat readable in peripheral vision.

### Notes
---
- Team Fortress 2-styled health indicators.
	- Health when damaged or buffed have noticeable indicators/icons.
- Indicators for current items/powerups.
	- Backpack is shown onscreen when in inventory.
	- Berserk is shown onscreen when picked up.

### Installation
---
- Load either the .ZIP or .PK3 with GZDoom.

### Compatibility
---
- Not compatible with anything that modifies the HUD. Especially since this uses SBARINFO which is extremely outdated.